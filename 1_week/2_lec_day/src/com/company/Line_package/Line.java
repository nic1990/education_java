package com.company.Line_package;

/*
Создайте класс отрезков на координатной плоскости, описав в нём все необходимые свойства, подобрав им понятные имена и правильные типы данных.
Опишите в классе конструктор, позволяющий при создании нового объекта явно задать все его свойства. Если это необходимо, то проверьте допустимость
их значений в конструкторе (например, в классе обыкновенных дробей нельзя создавать дробь с нулевым знаменателем).
Создайте в классе метод, проверяющий равна ли длина двух отрезков.
С использованием построенного класса создайте два отрезка: один от точки (1;1) до точки (2;2) и второй отрезок от точки (-3;0) до точки (1;1).
Проверьте с помощью созданного метода равна ли их длина и если равна, то выведите соответсвующее сообщение на экран.
*/

public class Line {
    private double x1;
    double y1;
    double x2;
    double y2;
    double len;

    public Line (double x1, double y1, double x2, double y2) throws BigLineException {
        if ((x1==x2)&&(y1==y2)) {
            String msg = String.format("Координаты равны, это точка : (x1, y1)(x2, y2) -> ( %.3f , %.3f ) ( %.3f , %.3f )",x1,y1,x2,y2);
            throw new BigLineException(msg);
        }
        this.x1=x1;
        this.y1=y1;
        this.x2=x2;
        this.y2=y2;
        this.len=Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    }

    public void show () {
        System.out.println(String.format("Отрезок. (x1, y1)(x2, y2) -> ( %.3f , %.3f ) ( %.3f , %.3f ) Длина = %.3f ",x1,y1,x2,y2,len));
    }

    public boolean cmpl (Line x2) {
        if (this.getLen() == x2.getLen()) {return true;}
        else {return false;}
    }

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
        this.len=Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
        this.len=Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    }

    public double getY1() {
        return y1;
    }

    public void setY1(double y1) {
        this.y1 = y1;
        this.len=Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    }

    public double getY2() {
        return y2;
    }

    public void setY2(double y2) {
        this.y2 = y2;
        this.len=Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    }

    public double getLen() {
        return len;
    }
}

