package com.company.Text_package;

public abstract class AbstractKeywordAnalyzer implements TextAnalyzer { // Абстрактный класс для анализа

    protected int txt_len=-1;
    protected String txt = null;

    public abstract String [] getKeywords() ; //возвращает набор ключевых слов для анализа (текст)

    public abstract Label getLabel() throws Exception; //возвращает метку, в случае, если р-тат анализа положительный

    @Override
    public Label processText(String text) throws Exception { //Анализирует текст. Не должен быть переопределен в наследниках
        this.txt_len=text.length();
        this.txt=text;
        try {
            return getLabel();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return Label.OK;
    }

    public static Label TextAnalyzis (AbstractKeywordAnalyzer TextAnalyzer[], String text ) {
        Label x = Label.OK;
        for (AbstractKeywordAnalyzer xs : TextAnalyzer ) {
            try {
                if (xs.processText(text)!=Label.OK) {
                    return xs.getLabel();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return x;
    }
}

/*
Необходимо реализовать иерархию классов.
Предположим, мы хотим анализироваьть текст, помечая его опрделенными метками. Метки заданы перечислением:

enum Label {
    SPAM, NEGATIVE_TEXT, TOO_LONG, OK
}
Общий интерфейс анализа текста:

interface TextAnalyzer {
    Label processText(String text);
}
Необходимо реализовать 3 класса, которые реализуют этот интерфейс: SpamAnalyzer, NegativeTextAnalyzer и TooLongTextAnalyzer и абстрактный класс AbstractKeywordAnalyzer

AbstractKeywordAnalyzer должен иметь 2 абстрактных метода: getKeywords(), который возвращает набор ключевых слов для анализа и getLabel(), который возвращает метку, в случае,
если р-тат анализа положительный. Так же на основе этих методов в этом классе должна быть реализован метод processText() и не должен быть реализован в наседниках.

Класс SpamAnalyzer должен наследоваться от KeywordAnalyzer, в нем должен быть конструктор, принимающий массив строк. Если одна из этих строк присутствует в проверямом
тексте, возвращаем Label.SPAM.

Класс NegativeTextAnalyzer должен наследоваться от KeywordAnalyzer и пусть анализирует тект на наличие одного из 3х смайликов: ":(", "=(", ":|" и возвращает
Label.NEGATIVE_TEXT, если р-тат положительный.

Класс TooLongTextAnalyzer в конструкторе должен принимать int и если длина     анализируемого текста больше этого значения, метод анализа должен возвращать label.TOO_LONG.
Во всех остальных случаях должен возвращаться Label.OK

Реализовать статический метод для проверки текста с 2-мя параметрами: TextAnalyzer[] - массив экземпляров классов для проверки текста, String - проверяемый текст, возвращать
должен Label - первую не-OK метку в порядке набора анализаторов текста, или OK, если все анализаторы вернули OK.

Рекомендации:
Можно пока не заморачиваться с проверкой допустимых значений.
Посмотрите в сторону String.contains().
Конечно, вы можете задавать в классах любые дополнительные поля и методы для вашего удобства. Однако, если это возможно, стоит их помечать приватными.
 */