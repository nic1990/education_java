package com.company.Text_package;

public class SpamAnalyzer extends AbstractKeywordAnalyzer {

    private String [] keys;

    public SpamAnalyzer(String[] keys) {
        this.keys=keys;
    }

    @Override
    public String [] getKeywords() {
        return keys;
    }

    @Override
    public Label getLabel() throws Exception {
        if (txt!=null) {
        for (String x1 : getKeywords()) {
            if (txt.contains(x1)) {
                return Label.SPAM;
            }
        }
        } else {
            NullPointerException e = new NullPointerException ("Передан объект нулевой длины");
            throw (e);
        }
        return Label.OK;
    }


}
