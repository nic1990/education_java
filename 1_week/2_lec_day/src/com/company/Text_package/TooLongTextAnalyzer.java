package com.company.Text_package;

public class TooLongTextAnalyzer extends AbstractKeywordAnalyzer {

    private int i=Integer.MAX_VALUE;

    public TooLongTextAnalyzer (int i) {
        this.i=i;
    }

    @Override
    public String[] getKeywords() {
        return null; //return new String[0];
    }

    @Override
    public Label getLabel() throws Exception {
        if (txt_len<0) {
            Exception e= new Exception ("Длина текста равна 0 !");
            throw (e);
        }
        if ((txt_len>i)) {
            return Label.TOO_LONG;
        }
        return Label.OK;
    }
}
