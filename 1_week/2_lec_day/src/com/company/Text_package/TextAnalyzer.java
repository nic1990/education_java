package com.company.Text_package;

public interface TextAnalyzer {
    Label processText(String text) throws Exception;
}
