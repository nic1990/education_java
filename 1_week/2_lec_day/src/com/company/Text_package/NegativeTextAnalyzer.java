package com.company.Text_package;

public class NegativeTextAnalyzer extends AbstractKeywordAnalyzer {

    private String [] smiles;

    public NegativeTextAnalyzer(String[] keys) {
        this.smiles=keys;
    }

    @Override
    public String[] getKeywords() {
        return smiles;
    }

    @Override
    public Label getLabel() throws Exception {
        if (txt!=null) {
            for (String x1 : getKeywords()) {
                if (txt.contains(x1)) {
                    return Label.NEGATIVE_TEXT;
                }
            }
        } else {
            NullPointerException e = new NullPointerException ("Передан объект нулевой длины");
            throw (e);
        }
        return Label.OK;
    }
}
