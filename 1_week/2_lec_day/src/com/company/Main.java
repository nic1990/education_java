package com.company;

import com.company.Line_package.BigLineException;
import com.company.Line_package.Line;
import com.company.Text_package.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        try {
            Line l1 = new Line(1, 1, 2, 2);
            Line l2 = new Line(-3, 0, 1, 1);
            l1.show();
            l2.show();
            System.out.println("Равны ли их длины ? - " + (l1.cmpl(l2) ? "ДА" : "НЕТ"));

            String text = "Это Текст :( для =( анализатора :| в нем Реклама для нас";

            String spam [] = {"Реклама", "для", "нас"};
            AbstractKeywordAnalyzer sa = new SpamAnalyzer(spam);

            int mlen = 10;
            AbstractKeywordAnalyzer tlta = new TooLongTextAnalyzer (mlen);

            String smiles [] = {":(", "=(", ":|"};
            AbstractKeywordAnalyzer nta = new NegativeTextAnalyzer(smiles);

            AbstractKeywordAnalyzer arr [] = { new NegativeTextAnalyzer(smiles), new TooLongTextAnalyzer (mlen), new SpamAnalyzer(spam) };
            Label mark1 = AbstractKeywordAnalyzer.TextAnalyzis(arr, text);
            System.out.println("Метка для  текста \""+text+"\"  = "+mark1);

            Line lx = new Line(3, 3, 3, 3);;
            lx.show();

        }catch (BigLineException ex) {
            System.out.println("Выбросилось исключение в catch ))))");
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        finally {
            System.out.println("А это блок finally");
        }
    }
}
