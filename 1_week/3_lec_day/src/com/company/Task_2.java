package com.company;

public class Task_2 {

    public static void main ( String [] args) {

        // Анонимный стиль реализации интерфейса
        Calculator calc = new Calculator() {
            @Override
            public double calculate(double y) {
                return 1;
            }
        };

        //Лямбда выражение
        integrate(x ->1, 0,10);

        System.out.println("Расчет площади прямоугольника методов квадратов Длина = 10, Высота = 1 ");

        System.out.println("Вызов анонимный стиль: Площадь = " + integrate(calc, 1, 10));

        System.out.println("Вызов люмбда-выражение: Площадь = " + Task_2.integrate(x ->1, 1,10));

    }

    static double integrate (Calculator f, double x1, double x2) {
        double summ = 0;
        for (double x=x1; x<x2; x+=0.1) {
            summ+=f.calculate(x)*0.1;
//          System.out.println(summ);
        }
        return summ;
    }
}
