package com.company;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Task_1 {

    public static void main(String[] args) {

        System.out.println("simplifyPath(\"/a/b/c/../d\") -> " + simplifyPath("/a/b/c/../d")); //→ "/a/b/d"
        System.out.println("simplifyPath(\"/a/\") -> " + simplifyPath("/a/")); //→ "/a"

        System.out.println("simplifyPath(\"/home/\") -> " + simplifyPath("/home/")); //→ "/home"
        System.out.println("simplifyPath(\"/a/./b/../../c/\") -> " + simplifyPath("/a/./b/../../c/")); //→ "/c"
        System.out.println("simplifyPath(\"/a/..\") -> " + simplifyPath("/a/..")); //→ "/"
        System.out.println("simplifyPath(\"/a/../\") -> " + simplifyPath("/a/../")); //→ "/"
        System.out.println("simplifyPath(\"/../../../../../a\") -> " + simplifyPath("/../../../../../a")); //→ "/a"
        System.out.println("simplifyPath(\"/a/./b/./c/./d/\") -> " + simplifyPath("/a/./b/./c/./d/")); //→ "/a/b/c/d"
        System.out.println("simplifyPath(\"/a/../.././../../.\") -> " + simplifyPath("/a/../.././../../.")); //→ "/"
        System.out.println("simplifyPath(\"/a//b//c//////d\") -> " + simplifyPath("/a//b//c//////d")); //→ "/a/b/c/d"

    }

    public static String simplifyPath (String path) {
        StringBuffer x = new StringBuffer();
        String[] numb = path.split("/");
        for (int y = 0; y < numb.length; y++) {
            if ((numb[y].equalsIgnoreCase("."))||(numb[y].equalsIgnoreCase(".."))||(numb[y].equalsIgnoreCase(""))) {
                /*
                */
            } else {
                if ((y<numb.length-1)) {
                    if ((numb[y+1].equalsIgnoreCase(".."))) { //(numb[y+1].equalsIgnoreCase("."))||
                    } else {
                        x.append("/").append(numb[y]);
                    }
                } else {
                    x.append("/").append(numb[y]);
                }
            }
        }
        if (x.toString().equalsIgnoreCase("")) {x.append("/");}
        return x.toString();
    }
}

/*
Упрощение путей:

Simplify the directory path (Unix like)
Given an absolute path for a file (Unix-style), simplify it. Note that absolute path always begin with ‘/’ ( root directory ), a dot in path represent current directory and double dot represents parent directory.

Examples:

"/a/./"   --> means stay at the current directory 'a'
"/a/b/.." --> means jump to the parent directory
              from 'b' to 'a'
"////"    --> consecutive multiple '/' are a  valid
              path, they are equivalent to single "/".

Input : /home/
Output : /home

Input : /a/./b/../../c/
Output : /c

Input : /a/..
Output : /

Input : /a/../
Ouput : /

Input : /../../../../../a
Ouput : /a

Input : /a/./b/./c/./d/
Ouput : /a/b/c/d

Input : /a/../.././../../.
Ouput : /

Input : /a//b//c//////d
Ouput : /a/b/c/d
*
* */
