package com.company;

import java.util.List;

public class Pair <T, E> {
    T t;
    E e;

    public Pair (T t, E e) {
        this.t=t;
        this.e=e;
    }

    public T getFirst () {
        return this.t;
    }

    public E getSecond () {
        return this.e;
    }

    public boolean equals (Pair x) {
        if ((this.getFirst().equals(x.getFirst()))&&(this.getSecond().equals(x.getSecond()))) {
            return true;
        } else {
            return false;
        }
    }
    public static <T, E> Pair of (Object t, Object e) {
        Pair <T, E>  x = new Pair(t,e);
        return x;
    }

    public static void main(String[] args) {

        Pair<Integer, String> pair1 = Pair.of(1, "hello");
        Integer i = pair1.getFirst(); //-> 1
        String s = pair1.getSecond(); //-> "hello"
        Pair<Integer, String> pair2 = Pair.of(1, "hello");
        Pair<Integer, String> pairWithNull = Pair.of(1, null);
        pair1.equals(pair2); //-> true
        pair1.equals(pairWithNull); //-> false

        System.out.println("Integer i = pair1.getFirst() ->"+pair1.getFirst());
        System.out.println("String s = pair1.getSecond() ->"+pair1.getSecond());

        System.out.println("pair1.equals(pair2) ->"+pair1.equals(pair2));
        System.out.println("pair1.equals(pairWithNull) ->"+pair1.equals(pairWithNull));
    }
}
