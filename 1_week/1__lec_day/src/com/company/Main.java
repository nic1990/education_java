package com.company;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {
	// write your code here
        // Main d = new Main ();
        System.out.println("Функция sleepIn:");
        System.out.println("sleepIn (false, false) ->"+ sleepIn (false, false));
        System.out.println("sleepIn (true, false) ->"+ sleepIn (true, false));
        System.out.println("sleepIn (false, true) ->"+ sleepIn (false, true));
        System.out.println("sleepIn (true, true) ->"+ sleepIn (true, true));
        System.out.println("Функция sumDouble:");
        System.out.println("sumDouble(2, 2) ->"+ sumDouble(1, 2));
        System.out.println("sumDouble(3, 2) ->"+ sumDouble(3, 2));
        System.out.println("sumDouble(3, 2) ->"+ sumDouble(2, 2));
        System.out.println("Функция altPairs:");
        System.out.println("altPairs(\"kitten\") -> "+altPairs("kitten"));
        System.out.println("altPairs(\"Chocolate\") -> "+altPairs("Chocolate"));
        System.out.println("altPairs(\"CodingHorror\") -> "+altPairs("CodingHorror"));
    }

    public static boolean sleepIn(boolean weekday, boolean vacation) { //Реализовать ф-цию. Параметр weekday принимает true, если сейчас будний день иначе выходные. Параметр vacation говорит, что мы в отпуске.
        if (vacation) { return true;}
        else {
            if (weekday) {return false;}
            else {return true;}
        }
    }
    public static int sumDouble(int a, int b) { //Реализовать ф-цию. Вернуть сумму чисел a и b, если они не равны. Иначе вернуть двойную сумму этих чисел.
        if (a!=b) {return a+b;}
        else {return 2*(a+b);}
    }
    public static String altPairs(String str) { //Реализовать ф-цию. Вернуть строку, которая состоит из пар индексов 0,1, 4,5, 8,9 и т.д.
        int [] index = {0,1,4,5,8,9,12,13,16,17,20,21,24,25,28,29,32,33,36,37,40,41,44,45,48,49,52,53};
        StringBuffer ns = new StringBuffer ();
        for (int i=0; (i<index.length) && (index[i]<str.length()); i++) {
            ns.append(str.charAt(index[i]));
        }
        return ns.toString();
    }

}
