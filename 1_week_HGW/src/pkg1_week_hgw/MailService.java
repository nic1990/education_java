/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1_week_hgw;

import java.util.function.Consumer;
import java.util.*;
import java.util.function.Supplier;

/**
 *
 * @author Amy
 */
public class MailService <T> implements Consumer  {
      
    public class NewMap <String, T> extends HashMap  { // Переопределяем в классе наследнике метод get для работы assert 
 
        @Override
        public List<T> get (Object t) {
            if (super.get(t)==null) {
                return new ArrayList <> ();
            } else {
              return (List<T>) super.get(t);   
            }
        }
    }
    
    private List <Message> x = new LinkedList <Message> (); // Список сообщений
    
    public Map<String, List<T>> getMailBox() {   
        NewMap <String, T> xc = new NewMap <> ();       
//      Map xc = new HashMap <String, List<T>> ();
        for (int i=0; i<x.size(); i++) {
            if (xc.get(x.get(i).To).isEmpty()) {
            List <T> xl = new ArrayList <T> ();
            xl.add((T) x.get(i).getContent());
            xc.put(x.get(i).To, xl);
            } else {
                List <T> xl = new ArrayList <T> ();
                xl = (List <T>) xc.get(x.get(i).To);
                xl.add((T) x.get(i).getContent());
                xc.put(x.get(i).To, xl);
            }         
        }
        return xc;
    }    

    @Override
    public void accept(Object t) {
        this.x.add((Message) t );
    }

    @Override
    public Consumer andThen(Consumer after) {
        return null;
    }
}
