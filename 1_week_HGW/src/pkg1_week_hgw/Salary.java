/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1_week_hgw;

/**
 *
 * @author Amy
 */
public class Salary extends Message {
    
    private int Content=-1;

    public Salary (String From, String To, int Content) {
        super (From, To);
        this.Content = Content;
    }
    
    @Override
    public Object getContent() {
        return Content;
    }
    
    
}
