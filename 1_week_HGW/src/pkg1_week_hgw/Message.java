/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1_week_hgw;

/**
 *
 * @author Amy
 */
public abstract class Message  { //implements Getable
    protected String From;
    protected String To;
    
    public String getFrom() {
        return this.From;
    }

    public String getTo() {
        return this.To;
    }

    public Message (String From, String To) {
    this.From = From;
    this.To = To;
    }
    
    public abstract Object getContent ();
}
