/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pkg1_week_hgw;

/**
 *
 * @author Amy
 */
public class MailMessage extends Message {
    
    private String Content;
    
    public MailMessage ( String From, String To, String Content) {
        super (From, To);
        this.Content = Content;
    }

    @Override
    public String getContent() {
        return this.Content;
    }
    
}
