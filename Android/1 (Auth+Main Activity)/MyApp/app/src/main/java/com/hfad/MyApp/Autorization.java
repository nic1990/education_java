package com.hfad.MyApp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.TextView;

import AppLogic.AutorizationManager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Autorization extends AppCompatActivity {

    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.textView5)
    TextView textView5;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.button)
    Button button;
    @BindView(R.id.button2)
    Button button2;
    @BindView(R.id.checkedTextView)
    CheckedTextView checkedTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autorization);
        ButterKnife.bind(this);
    }

    @OnClick (R.id.button)
    public void OnSingIn (View view) {
        if (AutorizationManager.auth(email.getText().toString(), password.getText().toString())){
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
