package com.hfad.MyApp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

import AppLogic.OffersToClients.BankOffer;
import AppLogic.OffersToClients.GetOffer;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.textView_type)
    TextView textView_type;
    @BindView(R.id.textView_description)
    TextView textView_description;
    @BindView(R.id.list_item)
    ListView listView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        BankOffer bf = GetOffer.getOffer(10);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date_begin = new Date(bf.getBegin().getTimeInMillis());

        textView2.setText(simpleDateFormat.format(date_begin));

        Date date_end = new Date(bf.getEnd().getTimeInMillis());
        textView3.setText(simpleDateFormat.format(date_end));
        textView.setText(bf.getSumm().toString());
        textView_type.setText(bf.getTitle().toString());
        textView_description.setText(bf.getDecription().toString());
        //listView
    }
}
