package AppLogic.OffersToClients;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class GetOffer {

    public static BankOffer getOffer (long client_id) {
        //Логика
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.MONTH, Calendar.MONTH+12);
        return new BankOffer("Кредит", "Выгодное предложение....", BigDecimal.valueOf(100500.0), new GregorianCalendar(), calendar);
    }
}
