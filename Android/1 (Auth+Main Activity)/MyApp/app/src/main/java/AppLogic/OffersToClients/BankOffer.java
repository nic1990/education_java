package AppLogic.OffersToClients;

import java.math.BigDecimal;
import java.util.Calendar;

public class BankOffer {
    protected BigDecimal summ;
    protected String title;
    protected String decription;
    protected Calendar begin;
    protected Calendar end;

    public String getTitle() {
        return title;
    }

    public String getDecription() {
        return decription;
    }

    public BigDecimal getSumm() {
        return summ;
    }

    public Calendar getBegin() {
        return begin;
    }

    public Calendar getEnd() {
        return end;
    }

    public BankOffer(String title, String decription, BigDecimal summ, Calendar begin, Calendar end) {
        this.title = title;
        this.decription = decription;
        this.summ = summ;
        this.begin = begin;
        this.end = end;
    }
}
