package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;


public class AdminPage extends MainPage {

    //Основная часть админской страницы

    @FindBy(id ="#content #tool #add-person span") //Кнопка Добавить человека
    private WebElement addPeople;

    public WebElement getAddPeople() {
        return addPeople;
    }
    //Остальное

    @FindBy(css ="#content #persons span") //Таблица людей на сервере
    private WebElement peopleTable;

    @FindBy(css ="#pages span")  //Доступные страницы с людьми на сервере
    private WebElement pageTable;

    @FindBy(css ="#pages-caption span")  //Тескст "Страницы" с теге pages
    private WebElement text;

 /*   @FindBy(xpath = "//input[@id = 'login-button']")
    private WebElement submitButton;
    */

    public AdminPage(WebDriver driver) {
        super(driver);
    }
}
