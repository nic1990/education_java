package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.sql.Driver;

public class LoginPage extends Page {

    //private WebDriver driver;
    public static final String URL = "http://at.pflb.ru/matrixboard2/";

    @FindBy(id ="login-username")
    private WebElement loginField;

    @FindBy(css ="input#login-password")
    private WebElement passwordField;

    @FindBy(xpath = "//input[@id = 'login-button']")
    private WebElement submitButton;

    public static LoginPage openPage(WebDriver driver)
    {
        driver.get(URL);
        return new LoginPage(driver);
    }
    public void fillLogin(String text)
    {
        loginField.sendKeys(text);
    }

    public void fillPassword(String text)
    {
        passwordField.sendKeys(text);
    }
    public void submit()
    {
        submitButton.click();
    }

    public LoginPage(WebDriver driver)
    {
        super(driver);
    }
}
