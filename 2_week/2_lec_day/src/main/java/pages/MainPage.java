package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.HashMap;
import java.util.Map;

public class MainPage extends Page {

    //Кнопка профиля и выход

    @FindBy(css="#profile span")    // Кнопка Профиля пользователя (и его имя)
    private WebElement userNameContainer;

    @FindBy(css="#logout span")    // Кнопка Выход
    private WebElement logoutbutton;

    //Меню - для всех страниц

    @FindBy(id ="#header #menu span") //Строка меню
    private WebElement allMenu;

    @FindBy(id ="#menu #logo") //Лого
    private WebElement menuLogo;

    @FindBy(id ="#menu .item a[href$=\"index.jsp\"]") //Кнопка "Люди"
    private WebElement menuPeople;

    @FindBy(id ="#menu .item a[href$=\"positions.jsp\"]") //Кнопка "Должности"
    private WebElement menuPos;

    @FindBy(id ="#menu .item a[href$=\"competencies.jsp\"]") //Кнопка "Компетенции"
    private WebElement menuCompet;

    @FindBy(id ="#menu .item a[href$=\"medals.jsp\"]") //Кнопка "Медали"
    private WebElement menuMedals;

    public MainPage(WebDriver driver) {
        super(driver);
    }

    //Инструменты
    //Поиск

    @FindBy(id ="#content #tools table tbody tr :nth-child(2) #query") //Поле поиск
    private WebElement queryField;

    @FindBy(css ="#content #tools table tbody tr :nth-child(2) #go span") //Кнопка Поиск
    private WebElement queryButton;



    //Выбор отдела

    @FindBy(id ="#content #tools table tbody :nth-child(2) td span") //Текст "Отдел"
    private WebElement addPeople;

    @FindBy(css ="#content #tools table tbody :nth-child(2) td #department") //Выпадающий список отделов
    private WebElement selDep;

    //Список отделов
    static Map <String, Integer> department= new HashMap <> ();

    public String getCurrentUsername(){
        return userNameContainer.getText();
    }

    public WebElement getDepartMenu (String txt ) {
        department.put("Все отделы", -1);
        department.put("QA", 35);
        department.put("Sale", 36);
        department.put("Infra", 38);
        department.put("Dev", 39);
        WebElement loginField = driver.findElement(By.id("#content #tools table tbody :nth-child(2) td #department option[value=\""+department.get(txt)+"\""));
        return loginField;
    }

    //Геттеры для элементов

    public WebElement getUserNameContainer() {
        return userNameContainer;
    }

    public WebElement getLogoutbutton() {
        return logoutbutton;
    }

    public WebElement getAllMenu() {
        return allMenu;
    }

    public WebElement getMenuLogo() {
        return menuLogo;
    }

    public WebElement getMenuPeople() {
        return menuPeople;
    }

    public WebElement getMenuPos() {
        return menuPos;
    }

    public WebElement getMenuCompet() {
        return menuCompet;
    }

    public WebElement getMenuMedals() {
        return menuMedals;
    }

    public WebElement getQueryField() {
        return queryField;
    }

    public WebElement getQueryButton() {
        return queryButton;
    }

}
