import helpers.Browser;
import helpers.DriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import pages.AdminPage;
import pages.LoginPage;
import pages.MainPage;

import java.util.concurrent.TimeUnit;

public class MatrixBoardTest {

    WebDriver driver;
    WebDriverWait wait;


    @BeforeTest
    public void start(){
        System.setProperty("webdriver.chrome.driver", "C:\\Driver\\chromedriver.exe");
        driver = DriverProvider.getDriver(Browser.CHROME);
        wait = new WebDriverWait(driver, 10);
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
    }

    @AfterTest
    public void tearDown(){
        driver.quit();
    }

    @Test
    public void loginTest(){
        //driver.get(URL);
        LoginPage loginPage = LoginPage.openPage(driver);

        /*WebElement loginField = driver.findElement(By.id("login-username"));
        WebElement passwordField = driver.findElement(By.id("login-password"));
        WebElement submitButton = driver.findElement(By.id("login-button"));*/


        /*loginField.sendKeys("user");
        passwordField.sendKeys("user");
        submitButton.click();*/

     /* loginPage.fillLogin("user");
        loginPage.fillPassword("user");
        loginPage.submit(); */

        loginPage.fillLogin("admin");
        loginPage.fillPassword("admin");
        loginPage.submit();

        MainPage mainPage = new MainPage(driver);


        //WebElement usernameContainer = driver.findElement(By.cssSelector("#profile span"));
        Assert.assertEquals(mainPage.getCurrentUsername(), "user", "authorization failed");
    }

    @Test
    public void addPeople () {
        AdminPage adminpage = new AdminPage (driver);
        adminpage.getAddPeople().click();
    }

    @Test
    public void logoutTest(){
        driver.findElement(By.cssSelector("#logout span")).click();
        Assert.assertTrue(driver.findElement(By.id("login-button")).isEnabled());
    }
}
